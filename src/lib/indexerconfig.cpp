/*
 * This file is part of the KDE Baloo Project
 * Copyright (C) 2014  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "indexerconfig.h"

#include <KConfig>
#include <KConfigGroup>
#include <Kdelibs4Migration>

#include <QDBusConnection>

using namespace Baloo;

class IndexerConfig::Private {
public:
};

IndexerConfig::IndexerConfig()
    : d(new Private)
{
}

IndexerConfig::~IndexerConfig()
{
    delete d;
}

bool IndexerConfig::fileIndexingEnabled() const
{
#if 0 // FIXME PORT
    return d->m_config.indexingEnabled();

#endif
    return true;
}


void IndexerConfig::setFileIndexingEnabled(bool enabled) const
{
    KConfig config(QStringLiteral("baloofilerc"));
    KConfigGroup basicSettings = config.group("Basic Settings");
    basicSettings.writeEntry("Indexing-Enabled", enabled);

    {
        Kdelibs4Migration kde4;
        KConfig config(kde4.locateLocal("config", QStringLiteral("baloofilerc")));
        KConfigGroup basicSettings = config.group("Basic Settings");
        basicSettings.writeEntry("Indexing-Enabled", enabled);
    }
}

bool IndexerConfig::shouldBeIndexed(const QString& ) const
{
#if 0 // FIXME PORT
    return d->m_config.shouldBeIndexed(path);
#endif
return true;
}

QStringList IndexerConfig::excludeFolders() const
{
#if 0 // FIXME PORT
    return d->m_config.excludeFolders();
#endif
    return QStringList();
}

QStringList IndexerConfig::includeFolders() const
{
#if 0 // FIXME PORT
    return d->m_config.includeFolders();
#endif
    return QStringList();
}

QStringList IndexerConfig::excludeFilters() const
{
#if 0 // FIXME PORT
    return d->m_config.excludeFilters();
#endif
    return QStringList();
}

QStringList IndexerConfig::excludeMimetypes() const
{
#if 0 // FIXME PORT
    return d->m_config.excludeMimetypes();
#endif
    return QStringList();
}

void IndexerConfig::setExcludeFolders(const QStringList& excludeFolders)
{
    KConfig config(QStringLiteral("baloofilerc"));
    config.group("General").writePathEntry("exclude folders", excludeFolders);

    {
        Kdelibs4Migration kde4;
        KConfig config(kde4.locateLocal("config", QStringLiteral("baloofilerc")));
        config.group("General").writePathEntry("exclude folders", excludeFolders);
    }
}

void IndexerConfig::setIncludeFolders(const QStringList& includeFolders)
{
    KConfig config(QStringLiteral("baloofilerc"));
    config.group("General").writePathEntry("folders", includeFolders);

    {
        Kdelibs4Migration kde4;
        KConfig config(kde4.locateLocal("config", QStringLiteral("baloofilerc")));
        config.group("General").writePathEntry("folders", includeFolders);
    }
}

void IndexerConfig::setExcludeFilters(const QStringList& excludeFilters)
{
    KConfig config(QStringLiteral("baloofilerc"));
    config.group("General").writeEntry("exclude filters", excludeFilters);

    {
        Kdelibs4Migration kde4;
        KConfig config(kde4.locateLocal("config", QStringLiteral("baloofilerc")));
        config.group("General").writeEntry("exclude filters", excludeFilters);
    }
}

void IndexerConfig::setExcludeMimetypes(const QStringList& excludeMimetypes)
{
    KConfig config(QStringLiteral("baloofilerc"));
    config.group("General").writeEntry("exclude mimetypes", excludeMimetypes);

    {
        Kdelibs4Migration kde4;
        KConfig config(kde4.locateLocal("config", QStringLiteral("baloofilerc")));
        config.group("General").writeEntry("exclude mimetypes", excludeMimetypes);
    }
}

bool IndexerConfig::firstRun() const
{
#if 0 // FIXME PORT
    return d->m_config.isInitialRun();
#endif
    return false;
}

void IndexerConfig::setFirstRun(bool firstRun) const
{
#if 0 // FIXME PORT
    d->m_config.setInitialRun(firstRun);
#endif
    {
        Kdelibs4Migration kde4;
        KConfig config(kde4.locateLocal("config", QStringLiteral("baloofilerc")));
        config.group("General").writeEntry("first run", firstRun);
    }
}

bool IndexerConfig::indexHidden() const
{
    KConfig config(QStringLiteral("baloofilerc"));
    return config.group("General").readEntry("index hidden folders", false);
}

void IndexerConfig::setIndexHidden(bool value) const
{
    KConfig config(QStringLiteral("baloofilerc"));
    config.group("General").writeEntry("index hidden folders", value);
}

bool IndexerConfig::onlyBasicIndexing() const
{
    KConfig config(QStringLiteral("baloofilerc"));
    return config.group("General").readEntry("only basic indexing", false);
}

void IndexerConfig::setOnlyBasicIndexing(bool value)
{
    KConfig config(QStringLiteral("baloofilerc"));
    config.group("General").writeEntry("only basic indexing", value);
}

void IndexerConfig::refresh() const
{
#if 0 // FIXME PORT
    org::kde::baloo::main mainInterface(QStringLiteral("org.kde.baloo"),
                                                QStringLiteral("/"),
                                                QDBusConnection::sessionBus());
    mainInterface.updateConfig();
#endif
}
