/*
 * This file is part of the KDE Baloo Project
 * Copyright (C) 2013-2015  Vishesh Handa <vhanda@kde.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "searchstore.h"
#include "term.h"

#include <QStandardPaths>
#include <QFile>
#include <QFileInfo>
#include <QUrl>

#include <KFileMetaData/PropertyInfo>
#include <KFileMetaData/TypeInfo>
#include <KFileMetaData/Types>

#include <tracker-sparql.h>

#include <algorithm>

using namespace Baloo;

SearchStore::SearchStore()
{
    m_prefixes.insert(QByteArray("filename"), QByteArray("F"));
    m_prefixes.insert(QByteArray("mimetype"), QByteArray("M"));
    m_prefixes.insert(QByteArray("rating"), QByteArray("R"));
    m_prefixes.insert(QByteArray("tag"), QByteArray("TA"));
    m_prefixes.insert(QByteArray("tags"), QByteArray("TA"));
    m_prefixes.insert(QByteArray("usercomment"), QByteArray("C"));
}

SearchStore::~SearchStore()
{
}

// Return the result with-in [offset, offset + limit)
QStringList SearchStore::exec(const Term& term, uint offset, int limit, bool sortResults)
{
  /* As we know only read-only queries will be done, it's enough
   * to use a connection with only direct-access setup. The NULL
   * represents a possible GCancellable.
   */
  GError *error = NULL;
  TrackerSparqlConnection *connection = tracker_sparql_connection_get (NULL, &error);
  if (!connection) {
    g_printerr ("Couldn't obtain a direct connection to the Tracker store: %s",
                error ? error->message : "unknown error");
    g_clear_error (&error);

    return QStringList();
  }

  qDebug() << term;

  /**
   * right hand side of query? do nothing
   */
  const QString rightQueryPart = constructQuery(term);
  if (rightQueryPart.isEmpty()) {
    return QStringList();
  }

  /**
   * sorting wanted? sort by modified
   */
  QString sorting;
  if (sortResults) {
      sorting = QStringLiteral(" ORDER BY DESC(nfo:fileLastModified(?u))");
  }

  /**
   * construct full query with where + sorting
   */
  const QString query(QStringLiteral("SELECT nie:url(?u) WHERE { ?u a nfo:FileDataObject . %1 }%2").arg(rightQueryPart).arg(sorting));

  qDebug() << "QUERY" << query;

  /* Make a synchronous query to the store */
  TrackerSparqlCursor *cursor = tracker_sparql_connection_query (connection,
                                            query.toUtf8().data(),
                                            NULL,
                                            &error);

  if (error) {
    /* Some error happened performing the query, not good */
    g_printerr ("Couldn't query the Tracker Store: '%s'",
                error ? error->message : "unknown error");
    g_clear_error (&error);

    return QStringList();
  }

  /* Check results... */
  QStringList matches;
  QSet<QString> guard;
  if (cursor) {
      /* Iterate, synchronously, the results... */
      while (tracker_sparql_cursor_next (cursor, NULL, &error)) {

      const QString match(QString::fromUtf8(tracker_sparql_cursor_get_string (cursor, 0, NULL)));
      if (match.startsWith(QStringLiteral("file://"))) {
        const QString fileName(match.mid(7));
        if (!guard.contains(fileName)) {
            matches << fileName;
            guard.insert(fileName);
        }
      }
    }
    g_object_unref (cursor);
  }

  g_object_unref (connection);

  /**
   * less matches than offset, be done with no results
   */
  if (offset >= uint(matches.size())) {
      return QStringList();
  }

  /**
   * apply offset and limit
   */
  return matches.mid(offset, limit);
}

QByteArray SearchStore::fetchPrefix(const QByteArray& property) const
{
    auto it = m_prefixes.constFind(property.toLower());
    if (it != m_prefixes.constEnd()) {
        return it.value();
    }
    else {
        KFileMetaData::PropertyInfo pi = KFileMetaData::PropertyInfo::fromName(property);
        if (pi.property() == KFileMetaData::Property::Empty) {
            qDebug() << "Property" << property << "not found";
            return QByteArray();
        }
        int propPrefix = static_cast<int>(pi.property());
        return 'X' + QByteArray::number(propPrefix) + '-';
    }

}

QString SearchStore::constructQuery(const Term& term)
{
    /**
     * 'and' and 'or' term combination
     */
    if (term.operation() == Term::And || term.operation() == Term::Or) {
        QString combinedQuery;
        const QList<Term> subTerms = term.subTerms();
        for (const Term& t : term.subTerms()) {
            const QString oneTerm(constructQuery(t));
            if (!oneTerm.isEmpty()) {
                if (combinedQuery.isEmpty())
                    combinedQuery = ((term.operation() == Term::And) ? QStringLiteral() : QStringLiteral("{ "));
                else {
                    combinedQuery += (term.operation() == Term::And) ? QStringLiteral(" . ") : QStringLiteral(" } UNION { ");
                }

                combinedQuery += oneTerm;
            }
        }

        if (combinedQuery.isEmpty()) {
            return QString();
        }

        return combinedQuery + ((term.operation() == Term::And) ? QStringLiteral() : QStringLiteral(" }"));
    }

    /**
     * nothing?
     */
    if (term.value().isNull()) {
        return QString();
    }

    /**
     * asserts already there in old baloo
     */
    Q_ASSERT(term.value().isValid());
    Q_ASSERT(term.comparator() != Term::Auto);
    Q_ASSERT(term.comparator() == Term::Contains ? term.value().type() == QVariant::String : true);

    /**
     * now: we shall have property + value
     */
    const QString property = term.property().toLower();
    const QVariant value = term.value();

    /**
     * includefolder => we want only stuff below the given folder!
     * implemented via FILTER
     */
    if (property == QLatin1String("includefolder")) {
        /**
         * construct filter, escape url
         */
        const QUrl folder = QUrl::fromLocalFile(QFileInfo(value.toString()).canonicalFilePath() + QLatin1Char('/'));
        auto literal = tracker_sparql_escape_string(folder.toEncoded().data());
        const QString result(QStringLiteral("FILTER(fn:starts-with(nie:url(?u), '%1'))").arg(QString::fromUtf8(literal)));
        g_free(literal);
        return result;
    }

    /**
     * document type
     * implemented via nfo:type
     */
    if (property == "type" || property == "kind") {
        /**
         * get type for nfo:
         */
        QString trackerType;
        switch (KFileMetaData::TypeInfo::fromName(value.toString()).type()) {
            case KFileMetaData::Type::Archive:
                trackerType = QStringLiteral("Archive");
                break;
            case KFileMetaData::Type::Audio:
                trackerType = QStringLiteral("Audio");
                break;
            case KFileMetaData::Type::Video:
                trackerType = QStringLiteral("Video");
                break;
            case KFileMetaData::Type::Image:
                trackerType = QStringLiteral("Image");
                break;
            case KFileMetaData::Type::Document:
                trackerType = QStringLiteral("Document");
                break;
            case KFileMetaData::Type::Spreadsheet:
                trackerType = QStringLiteral("Spreadsheet");
                break;
            case KFileMetaData::Type::Presentation:
                trackerType = QStringLiteral("Presentation");
                break;
            case KFileMetaData::Type::Text:
                trackerType = QStringLiteral("Text");
                break;
            case KFileMetaData::Type::Folder:
                trackerType = QStringLiteral("Folder");
                break;
            default:
                // no query possible
                return QString();
        }

        /**
         * construct match
         */
        return QStringLiteral("?u a nfo:%1").arg(trackerType);
    }

    /**
     * includefolder => we want only stuff below the given folder!
     * implemented via FILTER
     */
    if (property == "modified" || property == "mtime") {
        /**
         * range?
         */
        if (value.type() == QVariant::ByteArray) {
            QByteArray ba = value.toByteArray();
            Q_ASSERT(ba.size() >= 4);

            int year = ba.mid(0, 4).toInt();
            int month = ba.mid(4, 2).toInt();
            int day = ba.mid(6, 2).toInt();

            Q_ASSERT(year);

            // uses 0 to represent whole month or whole year
            month = month >= 0 && month <= 12 ? month : 0;
            day = day >= 0 && day <= 31 ? day : 0;

            QDate startDate(year, month ? month : 1, day ? day : 1);
            QDate endDate(startDate);

            if (month == 0) {
                endDate.setDate(endDate.year(), 12, 31);
            } else if (day == 0) {
                endDate.setDate(endDate.year(), endDate.month(), endDate.daysInMonth());
            }

            /**
             * recurse with new range term
             */
            Term dateRange(QStringLiteral("modified"), startDate, Term::GreaterEqual);
            dateRange = dateRange && Term(QStringLiteral("modified"), QDateTime(endDate, QTime(23, 59, 59)), Term::LessEqual);
            return constructQuery(dateRange);
        }

        /**
         * get date string for query or no query possible
         * TODO: better handling of pure dates, perhaps see them as 1 day interval
         */
        QString dateString;
        if (value.type() == QVariant::Date) {
            dateString = value.toDateTime().toString(Qt::ISODate);
        } else if (value.type() == QVariant::DateTime) {
            dateString = value.toDateTime().toString(Qt::ISODate);
        } else {
            return QString();
        }

        /**
         * get comparator
         */
        QString comparatorString;
        switch (term.comparator()) {
            case Term::Auto:
            case Term::Equal:
            case Term::Contains:
                comparatorString = QStringLiteral("=");
                break;
            case Term::Greater:
                comparatorString = QStringLiteral(">");
                break;
            case Term::GreaterEqual:
                comparatorString = QStringLiteral(">=");
                break;
            case Term::Less:
                comparatorString = QStringLiteral("<");
                break;
            case Term::LessEqual:
                comparatorString = QStringLiteral("<=");
                break;
        }

        /**
         * construct filter
         */
        return QStringLiteral("FILTER (nfo:fileLastModified(?u) %1 '%2')").arg(comparatorString).arg(dateString);
    }

    /**
     * tags?
     */
    if (property == "tag" || property == "tags") {
        /**
         * construct match, escape string
         */
        auto literal = tracker_sparql_escape_string(value.toString().toUtf8().data());
        const QString result(QStringLiteral("{ ?u a nfo:FileDataObject ; nao:hasTag [ nao:prefLabel '%1' ] }").arg(QString::fromUtf8(literal)));
        g_free(literal);
        return result;
    }

    /**
     * ATM handle contains and equal the same way
     */
    auto com = term.comparator();
    if (com == Term::Contains || com == Term::Equal) {
        /**
         * construct match, escape string
         */
        auto literal = tracker_sparql_escape_string(value.toString().toUtf8().data());
        const QString result(QStringLiteral("?u fts:match '%1'").arg(QString::fromUtf8(literal)));
        g_free(literal);
        return result;
    }

    // all other stuff ATM not supported

    return QString();


#if 0
    Q_ASSERT(tr);



    else if (property == "rating") {
        bool okay = false;
        int rating = value.toInt(&okay);
        if (!okay) {
            qDebug() << "Rating comparisons must be with an integer";
            return 0;
        }

        PostingDB::Comparator pcom;
        if (term.comparator() == Term::Greater || term.comparator() == Term::GreaterEqual) {
            pcom = PostingDB::GreaterEqual;
            if (term.comparator() == Term::Greater && rating)
                rating++;
        }
        else if (term.comparator() == Term::Less || term.comparator() == Term::LessEqual) {
            pcom = PostingDB::LessEqual;
            if (term.comparator() == Term::Less)
                rating--;
        }
        else if (term.comparator() == Term::Equal) {
            EngineQuery q = constructEqualsQuery("R", value.toString());
            return tr->postingIterator(q);
        }
        else {
            Q_ASSERT(0);
            return 0;
        }

        const QByteArray prefix = "R";
        const QByteArray val = QByteArray::number(rating);
        return tr->postingCompIterator(prefix, val, pcom);
    }

    QByteArray prefix;
    if (!property.isEmpty()) {
        prefix = fetchPrefix(property);
        if (prefix.isEmpty()) {
            return 0;
        }
    }

    auto com = term.comparator();
    if (com == Term::Contains) {
        EngineQuery q = constructContainsQuery(prefix, value.toString());
        return tr->postingIterator(q);
    }

    if (com == Term::Equal) {
        EngineQuery q = constructEqualsQuery(prefix, value.toString());
        return tr->postingIterator(q);
    }

    QVariant val = term.value();
    if (val.type() == QVariant::Int) {
        int intVal = value.toInt();

        PostingDB::Comparator pcom;
        if (term.comparator() == Term::Greater || term.comparator() == Term::GreaterEqual) {
            pcom = PostingDB::GreaterEqual;
            if (term.comparator() == Term::Greater && intVal)
                intVal++;
        }
        else if (term.comparator() == Term::Less || term.comparator() == Term::LessEqual) {
            pcom = PostingDB::LessEqual;
            if (term.comparator() == Term::Less)
                intVal--;
        }
        else {
            Q_ASSERT(0);
            return 0;
        }

        return tr->postingCompIterator(prefix, QByteArray::number(intVal), pcom);
    }

    return 0;
#endif
}

#if 0 // FIXME PORT
EngineQuery SearchStore::constructContainsQuery(const QByteArray& prefix, const QString& value)
{
    QueryParser parser;
    return parser.parseQuery(value, prefix);
}

EngineQuery SearchStore::constructEqualsQuery(const QByteArray& prefix, const QString& value)
{
    // We use the TermGenerator to normalize the words in the value and to
    // split it into other words. If we split the words, we then add them as a
    // phrase query.
    QStringList terms = TermGenerator::termList(value);

    QVector<EngineQuery> queries;
    int position = 1;
    for (const QString& term : terms) {
        QByteArray arr = prefix + term.toUtf8();
        queries << EngineQuery(arr, position++);
    }

    if (queries.isEmpty()) {
        return EngineQuery();
    } else if (queries.size() == 1) {
        return queries.first();
    } else {
        return EngineQuery(queries, EngineQuery::Phrase);
    }
}

EngineQuery SearchStore::constructTypeQuery(const QString& value)
{
    Q_ASSERT(!value.isEmpty());

    KFileMetaData::TypeInfo ti = KFileMetaData::TypeInfo::fromName(value);
    if (ti == KFileMetaData::Type::Empty) {
        qDebug() << "Type" << value << "does not exist";
        return EngineQuery();
    }
    int num = static_cast<int>(ti.type());

    return EngineQuery('T' + QByteArray::number(num));
}

PostingIterator* SearchStore::constructMTimeQuery(Transaction* tr, const QDateTime& dt, Term::Comparator com)
{
    Q_ASSERT(dt.isValid());
    quint32 timet = dt.toTime_t();

    MTimeDB::Comparator mtimeCom;
    if (com == Term::Equal) {
        mtimeCom = MTimeDB::Equal;
        quint32 end = QDateTime(dt.date().addDays(1)).toTime_t() - 1;

        return tr->mTimeRangeIter(timet, end);
    }
    else if (com == Term::GreaterEqual) {
        mtimeCom = MTimeDB::GreaterEqual;
    } else if (com == Term::Greater) {
        timet++;
        mtimeCom = MTimeDB::GreaterEqual;
    } else if (com == Term::LessEqual) {
        mtimeCom = MTimeDB::LessEqual;
    } else if (com == Term::Less) {
        mtimeCom = MTimeDB::LessEqual;
        timet--;
    } else {
        Q_ASSERT_X(0, "SearchStore::constructQuery", "mtime query must contain a valid comparator");
        return 0;
    }

    return tr->mTimeIter(timet, mtimeCom);
}

#endif
